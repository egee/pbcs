angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('FinalCtrl', function($scope) {

  $scope.showCard = false;
  $scope.listItems = [
    {
      url: 'weezer-blue-album',
      bandName : 'Weezer',
      albumName : 'Blue Album',
      albumImg : 'http://ionicframework.com/img/docs/blue-album.jpg'
    }, {
      url: 'sp-red-album',
      bandName : 'Smashing Punpkins',
      albumName : 'Red Album',
      albumImg : 'http://ionicframework.com/img/docs/siamese-dream.jpg'
    }
  ];

})

.controller('FinalDetailCtrl', function($scope, $stateParams, $cordovaActionSheet){

  var options = {
    title: 'What do you want with this image?',
    buttonLabels: ['Share via Facebook', 'Share via Twitter'],
    addCancelButtonWithLabel: 'Cancel',
    androidEnableCancelButton : true,
    winphoneEnableCancelButton : true,
    addDestructiveButtonWithLabel : 'Delete it'
  };


  document.addEventListener("deviceready", function () {

    $cordovaActionSheet.show(options)
      .then(function(btnIndex) {
        var index = btnIndex;
      });
  }, false);

})


























;
